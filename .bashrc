export IGNOREEOF=42
export HISTFILESIZE=
export HISTSIZE=
export HISTCONTROL=ignoredups:ignorespace

export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus

export MC_SKIN=$HOME/.mc/solarized.ini

export HUNTER_ROOT=/data/build/deps/hunter
export POLLY_ROOT=/data/build/deps/polly

shopt -s checkwinsize

#function _update_ps1() {
#    PS1="$(~/.dot/powerline-shell.py $? 2> /dev/null)"
#}
#
#if [ "$TERM" != "linux" ]; then
#    PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
#fi

source ~/.dot/.bash-powerline.sh
