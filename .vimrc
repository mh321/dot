source ~/.dot/vimrc/vimrcs/basic.vim
source ~/.dot/vimrc/vimrcs/plugins_config.vim

set relativenumber
set number
set t_Co=256

set background=dark

set nocompatible              " be iMproved, required
filetype off                  " required

" map to <Leader>cf in C++ code
autocmd FileType c,cpp,objc nnoremap <buffer><Leader>cf :<C-u>ClangFormat<CR>
autocmd FileType c,cpp,objc vnoremap <buffer><Leader>cf :ClangFormat<CR>
nmap <Leader>C :ClangFormatAutoToggle<CR>
let g:clang_format#detect_style_file = 1

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'ctrlpvim/ctrlp.vim'
Bundle 'Rykka/riv.vim'
Bundle 'altercation/vim-colors-solarized.git'
Plugin 'jlanzarotta/bufexplorer'
Plugin 'itchyny/lightline.vim'
Bundle 'tpope/vim-fugitive.git'
Bundle 'junegunn/goyo.vim'
Bundle 'amix/vim-zenroom2'
Plugin 'octol/vim-cpp-enhanced-highlight'
Bundle 'rhysd/vim-clang-format'
Bundle 'rdnetto/YCM-Generator'
"Bundle 'Lokaltog/vim-powerline'

call vundle#end()            " required
filetype plugin indent on    " required
colorscheme solarized 

